Ext.Loader.setConfig({enabled: true});

Ext.require('Ext.container.Viewport');

Ext.application({
    name: 'App',
    appFolder: 'js/app',
    controllers: ['Bugs'],
    models: ['bug.Status', 'bug.Type', 'bug.Priority', 'bug.Resolution', 'bug.Label', 'User'],
    stores: ['bug.Status', 'bug.Type', 'bug.Priority', 'bug.Resolution', 'bug.Label', 'Users'],

    launch: function() {
        Ext.create('Ext.panel.Panel', {
            renderTo: Ext.getElementById("mainContent"),
            autoCreateViewPort: false,
            layout: 'fit',
            id: "appContainer",
            frame: true,
            listeners: {
                beforerender: function () {
                    Ext.getCmp("appContainer").setHeight(Ext.getBody().getHeight() - 100);
                    Ext.getCmp("appContainer").doLayout();

                    Ext.EventManager.onWindowResize(function () {
                        Ext.getCmp("appContainer").setHeight(Ext.getBody().getHeight() - 100);
                        Ext.getCmp("appContainer").doLayout();
                    });
                }
            },
            items: [{
                layout: 'border',
                items: [{
                    layout  : 'fit',
                    region  :'center',
                    items: [{
                        layout: 'border',
                        items: [
                            {
                                xtype: 'bugtoolbar',
                                region: 'north'
                            },
                            {
                                xtype: 'bugslist',
                                region: 'west',
                                flex: 1
                            },
                            {
                                xtype: 'buginfo',
                                region: 'center',
                                flex: 2
                            }
                        ]
                    }]
                }]
            }]
        });
    }
});