Ext.define('App.controller.Bugs', {
    extend: 'Ext.app.Controller',

    stores: ['Bugs'],
    models: ['Bug'],
    views: ['bug.List','bug.Info','bug.Toolbar','bug.Create'],

    refs: [
        {
            selector: 'bugslist',
            ref: 'bugslist'
        },
        {
            selector: 'buginfo',
            ref: 'buginfo'
        },
        {
            selector: 'bugtoolbar',
            ref: 'bugtoolbar'
        },
        {
            selector: 'bugcreate',
            ref: 'bugcreate'
        }
    ],


    init: function() {
        this.getBugsStore().load();

        this.control({
            'bugslist dataview': {
                itemclick: this.bindBugToInfo
            },
            'bugtoolbar > button[action=create]': {
                click: this.onCreateBug
            },
            'bugtoolbar > form > button[action=filter]': {
                click: this.updateList
            },
            'bugcreate button[action=save]' : {
                click: this.createBug
            }
        });

    },

    onCreateBug: function () {
        var bugcreate = Ext.widget('bugcreate');
    },

    createBug: function(){
        var controller = this;
        var form = controller.getBugcreate().getForm();
        if (form.isValid()) {
            Ext.Ajax.request({
                url: '/api/bug',
                method: "POST",
                params: form.getValues(),
                success: function(response){
                    controller.updateList();
                }
            });
        }

    },

    bindBugToInfo: function(grid, record) {
        var controller = this;
        Ext.Ajax.request({
            url: '/api/bug/' + record.data.id,
            method: "GET",
            success: function(response){
                var jsonResponse = Ext.decode(response.responseText);
                controller.getBuginfo().updateInfo(jsonResponse.result);
            }
        });
    },

    updateList: function() {
        var controller = this;
        var params = this.getBugtoolbar().getFilterValue();
        var filterParams = {};

        Ext.iterate(params, function(key, value) {
            if(Array.isArray(value)){
                filterParams[key + '[]'] = [];
                Ext.Array.each(value, function(name, index) {
                    filterParams[key + '[]'].push(name);
                });
            } else {
                filterParams[key] = value;
            }
        }, this);

        Ext.Ajax.request({
            url: '/api/bug',
            method: "GET",
            params: filterParams,
            success: function(response){
                var jsonResponse = Ext.decode(response.responseText);
                controller.getBugslist().getStore().loadData(jsonResponse.results);
            }
        });

    }

});