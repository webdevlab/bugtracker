Ext.define('App.view.bug.List' ,{
    extend: 'Ext.grid.Panel',
    alias : 'widget.bugslist',
    border:  1,

    title: 'Bugs',

    initComponent: function() {
        this.store = 'Bugs';

        this.columns = [
            {text: "Title", flex: 1, dataIndex: 'title', sortable: true},
            {text: "Status", width: 115, dataIndex: 'status', sortable: true}
        ];

        this.viewConfig = {
            forceFit: true
        };

        this.callParent(arguments);
    }
});