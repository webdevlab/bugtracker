Ext.define('App.view.bug.Toolbar', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.bugtoolbar',
    initComponent: function () {
        this.callParent(arguments);
        this._form = Ext.create('Ext.form.Panel', {
            autoHeight: true,
            layout: 'hbox',
            bodyPadding: 10,
            items: [
                {
                    xtype: 'combobox',
                    store: Ext.getStore('bug.Type'),
                    emptyText:'Select type...',
                    multiSelect: true,
                    queryMode: 'local',
                    displayField: 'title',
                    valueField: 'id',
                    name: 'bugType'
                },{
                    xtype: 'combobox',
                    store: Ext.getStore('bug.Status'),
                    emptyText:'Select status...',
                    multiSelect: true,
                    queryMode: 'local',
                    displayField: 'title',
                    valueField: 'id',
                    name: 'bugStatus'
                },{
                    xtype: 'combobox',
                    store: Ext.getStore('Users'),
                    emptyText:'Select assignee...',
                    multiSelect: true,
                    queryMode: 'local',
                    displayField: 'displayName',
                    valueField: 'id',
                    name: 'assignee'
                },
                {
                    xtype: 'button',
                    text: 'Apply filter',
                    action: 'filter'
                }
            ]
        });

        this.add({
            xtype: 'button',
            text: 'Create Bug',
            action: 'create'
        });

        this.add(this._form);

    },
    getFilterValue: function(){
        return this._form.getForm().getValues();
    }
});