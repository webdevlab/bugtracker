Ext.define('App.view.bug.Info', {
    extend: 'Ext.Panel',
    alias: 'widget.buginfo',

    title: 'Bug Detail',

    tplMarkup: [
        '<div>',
        '<b>Title:</b> {title}<br/>',
        '<b>Status:</b> {status}<br/>',
        '<b>Priority:</b> {priority}<br/>',
        '<b>Type:</b> {type}<br/>',
        '<b>Resolution:</b> {resolution}<br/>',
        '<b>Assignee:</b> {assignee}<br/>',
        '<b>Description:</b> {description}<br/>',
        '</div>'
    ],
    startingMarkup: 'Select a bug to see additional information.',
    bodyPadding: 15,

    initComponent: function() {
        this.tpl = Ext.create('Ext.Template', this.tplMarkup);
        this.html = this.startingMarkup;

        this.bodyStyle = {
            background: '#ffffff'
        };
        this.callParent(arguments);
    },
    updateInfo: function(data) {
        this.tpl.overwrite(this.body, data);
    }
});