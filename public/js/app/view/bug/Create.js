Ext.define('App.view.bug.Create', {
    extend: 'Ext.window.Window',
    alias: 'widget.bugcreate',
    title: 'Create Bug',
    layout: 'fit',
    width: '40%',
    autoShow: true,
    initComponent: function () {
        this.items =  [
            {
                xtype: 'form',
                bodyPadding: 10,
                defaults: {
                    labelAlign: 'left',
                    anchor: '100%'
                },
                items: [
                    {
                        xtype: 'textfield',
                        name: 'title',
                        fieldLabel: 'Title',
                        allowBlank: false
                    },
                    {
                        xtype: 'textarea',
                        name: 'description',
                        fieldLabel: 'Description'
                    },
                    {
                        xtype: 'combobox',
                        store: Ext.getStore('bug.Type'),
                        emptyText:'Select type...',
                        queryMode: 'local',
                        displayField: 'title',
                        valueField: 'id',
                        name: 'bugType',
                        fieldLabel: 'Type',
                        allowBlank: false
                    },{
                        xtype: 'combobox',
                        store: Ext.getStore('bug.Status'),
                        emptyText:'Select status...',
                        queryMode: 'local',
                        displayField: 'title',
                        valueField: 'id',
                        name: 'bugStatus',
                        fieldLabel: 'Status',
                        allowBlank: false
                    },{
                        xtype: 'combobox',
                        store: Ext.getStore('Users'),
                        emptyText:'Select assignee...',
                        queryMode: 'local',
                        displayField: 'displayName',
                        valueField: 'id',
                        name: 'assignee',
                        fieldLabel: 'Assignee',
                        allowBlank: false
                    },{
                        xtype: 'combobox',
                        store: Ext.getStore('Users'),
                        emptyText:'Select reporter...',
                        queryMode: 'local',
                        displayField: 'displayName',
                        valueField: 'id',
                        name: 'reporter',
                        fieldLabel: 'Reporter',
                        allowBlank: false
                    },{
                        xtype: 'combobox',
                        store: Ext.getStore('bug.Priority'),
                        emptyText:'Select priority...',
                        queryMode: 'local',
                        displayField: 'title',
                        valueField: 'id',
                        name: 'priority',
                        fieldLabel: 'Priority',
                        allowBlank: false
                    },{
                        xtype: 'combobox',
                        store: Ext.getStore('bug.Resolution'),
                        emptyText:'Select resolution...',
                        queryMode: 'local',
                        displayField: 'title',
                        valueField: 'id',
                        name: 'resolution',
                        fieldLabel: 'Resolution'
                    },{
                        xtype: 'combobox',
                        store: Ext.getStore('bug.Label'),
                        emptyText:'Select labels...',
                        queryMode: 'local',
                        multiSelect: true,
                        displayField: 'title',
                        valueField: 'id',
                        name: 'labels',
                        fieldLabel: 'Labels'
                    }
                ]
            }
        ];
        this.buttons = [
            {
                text: 'Save',
                action: 'save'
            },
            {
                text: 'Cancel',
                scope: this,
                handler: this.close
            }
        ];
        this.callParent(arguments);
    },
    getForm: function(){
        return this.down('form');
    }
});