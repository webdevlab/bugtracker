Ext.define('App.model.bug.Type',{
	extend: 'Ext.data.Model',
	fields: ['id','title']
});