Ext.define('App.model.bug.Label',{
    extend: 'Ext.data.Model',
	fields: ['id','title']
});