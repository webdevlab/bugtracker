Ext.define('App.model.Bug', {
    extend: 'Ext.data.Model',
    fields: ['id', 'title', 'description', 'status']
});