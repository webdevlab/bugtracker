Ext.define('App.store.bug.Status', {
    extend: 'Ext.data.Store',	
    model: 'App.model.bug.Status',
    autoLoad: true,
	proxy: {
		type: 'ajax',
		url: '/api/bug-status',
        reader: {
            type: 'json',
            root: 'results'
        }
	}
});