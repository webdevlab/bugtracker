Ext.define('App.store.bug.Label', {
    extend: 'Ext.data.Store',	
    model: 'App.model.bug.Label',
    autoLoad: true,
	proxy: {
		type: 'ajax',
		url: '/api/bug-label',
        reader: {
            type: 'json',
            root: 'results'
        }
	}
});