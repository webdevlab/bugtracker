Ext.define('App.store.bug.Resolution', {
    extend: 'Ext.data.Store',	
    model: 'App.model.bug.Resolution',
    autoLoad: true,
	proxy: {
		type: 'ajax',
		url: '/api/bug-resolution',
        reader: {
            type: 'json',
            root: 'results'
        }
	}
});