Ext.define('App.store.bug.Priority', {
    extend: 'Ext.data.Store',	
    model: 'App.model.bug.Priority',
    autoLoad: true,
	proxy: {
		type: 'ajax',
		url: '/api/bug-priority',
        reader: {
            type: 'json',
            root: 'results'
        }
	}
});