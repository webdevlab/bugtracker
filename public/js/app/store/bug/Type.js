Ext.define('App.store.bug.Type', {
    extend: 'Ext.data.Store',	
    model: 'App.model.bug.Type',
    autoLoad: true,
	proxy: {
		type: 'ajax',
		url: '/api/bug-type',
        reader: {
            type: 'json',
            root: 'results'
        }
	}
});