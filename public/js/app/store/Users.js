Ext.define('App.store.Users', {
    extend: 'Ext.data.Store',	
    model: 'App.model.User',
    autoLoad: true,
	proxy: {
		type: 'ajax',
		url: 'api/user',
        reader: {
            type: 'json',
            root: 'results'
        }
	}
});