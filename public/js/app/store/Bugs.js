Ext.define('App.store.Bugs', {
    extend: 'Ext.data.Store',
    model: 'App.model.Bug',
    proxy: {
        type: 'ajax',
        url: '/api/bug',
        reader: {
            type: 'json',
            root: 'results'
        },
        autoLoad: true
    }
});