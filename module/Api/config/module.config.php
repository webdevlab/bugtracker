<?php

namespace Api;

return array(
    'router' => array(
        'routes' => array(
            'bug' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/api/bug[/][:id]',
                    'constraints' => array(
                        'id'     => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Api\Controller\Bug',
                    ),
                ),
            ),
            'user' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/api/user[/]',
                    'defaults' => array(
                        'controller' => 'Api\Controller\User',
                    ),
                ),
            ),
            'bug-status' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/api/bug-status[/]',
                    'defaults' => array(
                        'controller' => 'Api\Controller\Status',
                    ),
                ),
            ),
            'bug-type' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/api/bug-type[/]',
                    'defaults' => array(
                        'controller' => 'Api\Controller\Type',
                    ),
                ),
            ),
            'bug-priority' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/api/bug-priority[/]',
                    'defaults' => array(
                        'controller' => 'Api\Controller\Priority',
                    ),
                ),
            ),
            'bug-resolution' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/api/bug-resolution[/]',
                    'defaults' => array(
                        'controller' => 'Api\Controller\Resolution',
                    ),
                ),
            ),
            'bug-label' => array(
                'type'    => 'segment',
                'options' => array(
                    'route'    => '/api/bug-label[/]',
                    'defaults' => array(
                        'controller' => 'Api\Controller\Label',
                    ),
                ),
            ),
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            __NAMESPACE__ . '_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                )
            )
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Api\Controller\Bug' => 'Api\Controller\BugController',
            'Api\Controller\Status' => 'Api\Controller\StatusController',
            'Api\Controller\Type' => 'Api\Controller\TypeController',
            'Api\Controller\Priority' => 'Api\Controller\PriorityController',
            'Api\Controller\Resolution' => 'Api\Controller\ResolutionController',
            'Api\Controller\Label' => 'Api\Controller\LabelController',
            'Api\Controller\User' => 'Api\Controller\UserController',
        ),
    ),
    'view_manager' => array(
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
);