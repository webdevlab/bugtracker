<?php
namespace Api\Controller;

use Api\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class TypeController extends AbstractApiController
{

    public function getList()
    {
        return $this->getJsonList('Api\Entity\Type');
    }

}