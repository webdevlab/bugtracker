<?php
namespace Api\Controller;

use Api\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class UserController extends AbstractApiController
{

    public function getList()
    {
        return $this->getJsonList('Application\Entity\User');
    }

}