<?php
namespace Api\Controller;

use Api\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class ResolutionController extends AbstractApiController
{

    public function getList()
    {
        return $this->getJsonList('Api\Entity\Resolution');
    }

}