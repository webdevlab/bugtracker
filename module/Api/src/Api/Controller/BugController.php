<?php
namespace Api\Controller;

use Api\Controller\AbstractRestfulController;
use Doctrine\Common\Collections\Criteria;
use Zend\View\Model\JsonModel;

class BugController extends AbstractApiController
{
    const ENTITY  = 'Api\Entity\Bug';

    public function getList()
    {
        $queryParams = $this->params()->fromQuery();
        $needArray = array('bugStatus'=>'', 'bugType'=>'', 'assignee'=>'');
        $filterParams = array_intersect_key($queryParams, $needArray);

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $bugCollection = $em->getRepository(self::ENTITY);

        if(count($filterParams)){
            $criteria = Criteria::create();

            if(isset($filterParams['bugStatus']) && is_array($filterParams['bugStatus'])) {
                $criteria->andWhere(Criteria::expr()->in("status", $filterParams['bugStatus']));
            }

            if(isset($filterParams['bugType']) && is_array($filterParams['bugType'])) {
                $criteria->andWhere(Criteria::expr()->in("type", $filterParams['bugType']));
            }

            if(isset($filterParams['assignee']) && is_array($filterParams['assignee'])) {
                $criteria->andWhere(Criteria::expr()->in("assignee", $filterParams['assignee']));
            }

            $bugCollection = $bugCollection->matching($criteria);
        } else {
            $bugCollection = $bugCollection->findAll();
        }

        $results = array();
        foreach ($bugCollection as $item) {
            $data = array();
            $data['id'] = $item->getId();
            $data['title'] = $item->getTitle();
            $data['status'] = $item->getStatus()->getTitle();
            $data['params'] =  $filterParams;
            $results[] = $data;
        }

        return new JsonModel(
            array(
                'success' => True,
                'results' => $results
            )
        );
    }

    public function get($id)
    {
        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $entity = $em->find(self::ENTITY, $id);

        $result = array();
        $result['id'] = $entity->getId();
        $result['title'] = $entity->getTitle();
        $result['description'] = $entity->getDescription();
        $result['status'] = $entity->getStatus()->getTitle();
        $result['type'] = $entity->getType()->getTitle();
        $result['priority'] = $entity->getPriority()->getTitle();
        $result['resolution'] = ($entity->getResolution())? $entity->getResolution()->getTitle() : Null;
        $result['assignee'] = ($entity->getAssignee())? $entity->getAssignee()->getDisplayName() : Null;

        return new JsonModel(
            array(
                'success' => True,
                'result' => $result
            )
        );
    }

    public function create($data)
    {   // Action used for POST requests
        return new JsonModel(array('data' => array('id'=> 3, 'name' => 'New Album', 'band' => 'New Band')));
    }

    public function update($id, $data)
    {   // Action used for PUT requests
        return new JsonModel(array('data' => array('id'=> 3, 'name' => 'Updated Album', 'band' => 'Updated Band')));
    }

    public function delete($id)
    {   // Action used for DELETE requests
        return new JsonModel(array('data' => 'album id 3 deleted'));
    }
}