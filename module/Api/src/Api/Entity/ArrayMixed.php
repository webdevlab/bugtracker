<?php
namespace Api\Entity;

trait ArrayMixed {
    public function toArray()
    {
        return get_object_vars($this);
    }
}