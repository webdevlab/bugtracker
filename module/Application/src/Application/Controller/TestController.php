<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class TestController extends AbstractActionController
{
    public function indexAction()
    {

        $em = $this->getServiceLocator()->get('Doctrine\ORM\EntityManager');
        $collection = $em->getRepository('Api\Entity\Bug')->findAll();

        //var_dump($collection);

        foreach ($collection as $item) {
            var_dump($item->getReporter()->get());
        }

        exit();

        return new ViewModel(
            array(
                'success' => true,
                'results' => $collection
            )
        );
    }
}
