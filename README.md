BugTracker
=======================

Обзор
------------
Простой баг трекер созданый в целях демонстрации работы связки ZF2 & ExtJS 4

Установка
------------

Зависимости в проекте устанавливаются с помощью менеджера зивисимостей `Сomposer`

Для начала нам нужно проверить что наш менеджер зависисмостей up-to-date. 
Для этого вызываем:

    cd my/project/dir
    php composer.phar self-update

Далее устанавливаем нужные для работы зависимости:

    php composer.phar install

Установка базы данных:
----------------
Создаем базу данных для проекта (по умочанию `bugtracker`):

    CREATE DATABASE IF NOT EXISTS `bugtracker` /*!40100 DEFAULT CHARACTER SET utf8 */;

Если название вашей базы отличное от той что указана по молчанию.
Вам нужно изменить параметры указаный в файле конфигурации доступа к базе:

    doctrine.global.php
    
Или же создать копию файла с именем:

    doctrine.local.php
    
и произвести изменения там.

Далее в корне проекта запускаем валидацию схем:

    ./vendor/bin/doctrine-module orm:validate-schema

в результате должны увидеть

    [Mapping]  OK - The mapping files are correct.
    [Database] FAIL - The database schema is not in sync with the current mapping file.
    
Генерируем схемы базы данных:
     
    ./vendor/bin/doctrine-module orm:schema-tool:create

Ожидаемый результат выполнения команды:

    Creating database schema...
    Database schema created successfully!

Далее импотируем тестовые данные:

    ./vendor/bin/doctrine-module dbal:import ./data/test_db.sql

Запуск сервера
----------------

### PHP CLI Server

Простой способ запуска это запустить встроеный PHP cli-server в корневой директории проекта:

    php -S 127.0.0.1:8080 -t public public/index.php

Сервер будет запущен на порте 8080.

**Заметка: ** Встроеный CLI сервер использовать *только для разработки*.

### Apache Setup

Для запуска через apache, настройте virtual host на public/ директорию проекта 
и вы будете готовы для работы с проектом! Настройка должна выглядеть примерно так:

    <VirtualHost *:80>
        ServerName bugtracker.loc
        DocumentRoot /path/to/bugtracker/public
        SetEnv APPLICATION_ENV "development"
        <Directory /path/to/bugtracker/public>
            DirectoryIndex index.php
            AllowOverride All
            Order allow,deny
            Allow from all
        </Directory>
    </VirtualHost>
    
**Заметка: ** Если вы хотите иметь доступ по url http://bugtracker.loc. 
Не за будьте добавить доменое имя в файл hosts который находиться в директории: 
 
    С:\Windows\System32\drivers\etc\

Проект готов для запуска.

Удачного дня!
