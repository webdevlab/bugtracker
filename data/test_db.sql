/* dump for status table */
INSERT INTO `status` (`id`, `title`) VALUES (1, 'Open');
INSERT INTO `status` (`id`, `title`) VALUES (2, 'Development');
INSERT INTO `status` (`id`, `title`) VALUES (3, 'Resolved');
INSERT INTO `status` (`id`, `title`) VALUES (4, 'Closed');

/* dump for type table */
INSERT INTO `type` (`id`, `title`) VALUES (1, 'Bug');
INSERT INTO `type` (`id`, `title`) VALUES (2, 'Task');
INSERT INTO `type` (`id`, `title`) VALUES (3, 'New Feature');

/* dump for priority table */
INSERT INTO `priority` (`id`, `title`) VALUES (1, 'Blocker');
INSERT INTO `priority` (`id`, `title`) VALUES (2, 'Critical');
INSERT INTO `priority` (`id`, `title`) VALUES (3, 'Major');
INSERT INTO `priority` (`id`, `title`) VALUES (4, 'Minor');
INSERT INTO `priority` (`id`, `title`) VALUES (5, 'Trivial');

/* dump for resolution table */
INSERT INTO `resolution` (`id`, `title`) VALUES (1, 'Fixed');
INSERT INTO `resolution` (`id`, `title`) VALUES (2, 'Won\'t fix');
INSERT INTO `resolution` (`id`, `title`) VALUES (3, 'Duplicate');
INSERT INTO `resolution` (`id`, `title`) VALUES (4, 'Incomplete');
INSERT INTO `resolution` (`id`, `title`) VALUES (5, 'Cannot Reproduce');

/* dump for users table */
INSERT INTO `users` (`id`, `username`, `email`, `displayName`, `password`)
VALUES (1, 'DangelZM', 'dangelzm@gmail.com', 'Ivan Zmerzlyi', '$2y$14$5EjjspjjI60IjmrNhMMjzOl9LYvOZSgoNXBDdKrfxWHO8BKwoyqha');
INSERT INTO `users` (`id`, `username`, `email`, `displayName`, `password`)
VALUES (2, 'User', 'demo.user@gmail.com', 'Demo User', '$2y$14$V1k.TX9Ydd7rJWexkIClTeyAmCZxAAmtziiCFlSk7zfIFW/0CS.9K');

/* dump for bug table */
INSERT INTO `bug` (`id`, `title`, `description`, `status_id`, `type_id`, `priority_id`, `resolution_id`, `reporter_id`, `assignee_id`)
VALUES (1, 'Первый демо баг', 'Описание для первого демо бага', 1, 1, 1, NULL, 1, 1);
INSERT INTO `bug` (`id`, `title`, `description`, `status_id`, `type_id`, `priority_id`, `resolution_id`, `reporter_id`, `assignee_id`)
VALUES (2, 'Второй демо баг', 'Описание для второго демо бага', 2, 2, 3, NULL, 2, 2);
INSERT INTO `bug` (`id`, `title`, `description`, `status_id`, `type_id`, `priority_id`, `resolution_id`, `reporter_id`, `assignee_id`)
VALUES (3, 'Третий демо баг', 'Описание для третего демо бага', 3, 3, 4, 1, 2, 1);

